/*global define, require*/
define(['jquery', 'underscore'], function($, _) {
    "use strict";

    // Interface
    var zugzug = {
        // application methods
        app : {},

        // module methods
        modules : {}
    };

    //debug mode?
    zugzug.debug = window._debug;

    // Loaded modules and their callbacks
    var _modules = {};

    //extensions object: not using this yet
    // var _extensions = {};

    // an array of modules that should be excluded
    var _excluded = [];

    /**
     * @class Module class that gets instatiated and is used for
     * lifecycle management by the core module
     *
     * @param {object} moduleOptions    options that we'll pass along to the class
     *                                  and this.module object
     *
     * @param {obect} moduleObj         the module object is the object interface
     *                                  returned by the main module
     */
    function Module(moduleOptions, moduleObj) {

        this.options        = _.extend({}, moduleOptions);

        this.$el            = this.options.$el;
        this.name           = this.options.name;
        this._id            = _.uniqueId(this.name + '_module_');

        this.module         = _.extend(moduleObj, moduleOptions);

        return this;
    }


    /**
     * Utility to check 
     * @param  {string}     name  name of the module to check for
     * @return {boolean}          true if module is excluded, false if it isn't
     */
    function checkExcluded(name){
        return _.find(_excluded, function(moduleName){return moduleName === name;}) ? true : false;
    }

     /**
     * INTERNAL METHODS
     */

    /**
     * load a module from a config object, the module corresponds
     * to a require module and executes the init method
     * 
     * @param  {object} module [options object that contains the options for this module]
     * @return {object}        [return a promise that is resolved when require has finished loading the module]
     */
    function loadModule(module) {

        if(!_.isObject(module)) {
            throw 'expected an object, instead found ' + (typeof module) + '';
        }

        if(!module.name || typeof module.name !== 'string') {
            throw 'module name is either missing or is not a string';
        }

        if(!module.$el) {
            //console.warn('Module [', module.name ,'] was not provided with an [ $el ], the module might require one to be added to the DOM');
        }


        // use the module name as the alias for the require call, currently
        // this must match the folder and file names of the module
        var moduleAlias = module.name;

        //deferred object to ensure the module is done loading before calling methods on it
        var $dfd = $.Deferred();


        // the module config object is really the setup options for the module.
        // there might be a better way of doing this...
        var moduleOptions = module;

        
        // if we're excluding a module just skip the module and return the promise
        // if (_.find(_excluded, function(moduleName){return moduleName === module.name;})) {
            
        //     // if we made it here, we're off to the races...
        //     // console.log('Module [', module.name ,']: excluded ');

        //     return $dfd.promise();
        // }

        // require call for the module interface
        require(['modules/' + moduleAlias + '/' + moduleAlias + '.main'], function(moduleObj){

            // add to the modules object
            _modules[ moduleAlias ] = new Module(moduleOptions, moduleObj);

            // resolve the defered object when the module is done being required
            // and pass it along for any deferred callbacks
            $dfd.resolve(_modules[ moduleAlias ]);
        });


        // return promise
        return $dfd.promise();

    }




    /**
     * API METHODS
     */

    
    /**
     * starts the application: loops through the data-module
     * dom nodes and loads each module it finds on the pgae
     * @return {[type]} [description]
     */
    zugzug.app.start = function() {
        var $dataModules = $('[data-module]');
        var total = $dataModules.length;
        var count = 0;
        var $appDfd = $.Deferred();

        // if there are any elements on the page with data-module
        // attributes, loop over them and load them
        if (total) {
            
            _.each($dataModules, function(module){
                var moduleName = $(module).data('module');

                if(checkExcluded(moduleName)){
                    count += 1;

                    if (count === total) {

                        $appDfd.resolve(zugzug);

                    }

                    return false;
                } 
                // load the modules into the queue 
                loadModule({

                    //name must match the folder and file name currently :(
                    name : $(module).data('module'),

                    // pass the container as the modules $el
                    $el: $(module)

                // load module returns a promise, when that's resolved attempt
                // to fire the modules init function
                }).done(

                    // the deferred object passes the module instance
                    // when it is resolved 
                    function(module){
                        
                        try {
                            
                            module.module.init();

                            // if we made it here, we're off to the races...
                            // console.log('Module [', module.name ,']: Loaded ');
                       
                        } catch(e) {

                            // enforce an init method
                            if(!module.module.init) {
                                throw new Error('Module [ ' + module.name + ' ]: is missing method: init');
                            }

                            throw new Error('Module [' + module.name  +']: failed to load: ' + e.message);



                        }

                        count += 1;

                        if (count === total) {

                            $appDfd.resolve(zugzug);

                        }
                        
                    }

                   

                );
            });
        }

        return $appDfd;

    };



    /**
     * exclude a module from loading even if specified. useful for specific builds
     * that require a module not to be loaded to function correctly. Call before any
     * start or load methods
     * 
     * @param  {string} moduleName 
     */
    zugzug.modules.exclude = function(moduleName) {
        if(_.isString(moduleName)) {
            _excluded.push(moduleName);
        } else {
            return false;
        }
        
    };

    zugzug.modules.start = function(moduleOpts, callback) {
        if(typeof moduleOpts === 'undefined') {
            throw 'An options object is required';
        }

        if(checkExcluded(moduleOpts.name)){

            if(typeof callback === 'function') {
               callback(moduleOpts); 
            }

            return false;


        }

         loadModule(moduleOpts).done(

            // the deferred object passes the module instance
            // when it is resolved
            function(module){

                try {
                    // attempt to init
                    module.module.init();

                    if(typeof callback === 'function') {
                       callback(module); 
                    }

                    // if we made it here, we're off to the races...
                    // console.log('Module [', module.name ,']: Loaded ');

                } catch(e) {

                    //enforce an init method
                    if(!module.module.init) {
                        throw new Error('Module [ ' + module.name + ' ]: is missing method: init');
                    }

                    throw new Error('Module [' + module.name + ']: failed to load: ' + e.message);



                }

            }
        );

    };

    
    /**
     * get a module by its name from the modules hash
     * 
     * @param  {[type]} moduleName [description]
     * @return {[type]}            [description]
     */
    zugzug.modules.get = function(moduleName) {
        if(moduleName && _modules[moduleName]) {
        
            return _modules[moduleName];

        } else if(!moduleName){

            return _modules;
            
        } else {

            // throw new Error(moduleName  + ': Hasn\'t been initialized');
            return false;
        }


    };

    return zugzug;

});
