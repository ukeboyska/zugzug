# Zugzug.js

## version: 0.1.2

## Demos are currently NOT WORKING:
They are a work in progress and hopefully we'll be able to put together some useful examples soon.

## What is it?

A core architecture for loading javascript modules and managing module life cycles.

## Why Are we making it?

1. To stop reinventing the wheel for projects that would follow a similar pattern of module loading.
2. To help speed up architecture design for projects.
3. To create a system for abstracting away architecture and allow for focus on project specific problems.

## Current Dependencies
Currently this architecture has dependencies on a few libraries. Current dependincies are:

1. jQuery (Dom manipulation and AJAX)
2. underscore (array, object, and function helper methods)

# Patterns
## Constructor pattern for a module:
The Pattren I've used the most has a ``moduleName.class.js`` file which holds the contructor function for a module:
```
// moduleName.class.js
define([], function(){

	var MyModule = function(options) {
		this.name = options.name	
	};

	MyModule.prototype.getName = function(){
		return this.name	
	};

	return MyModule;
	
});

```

Then the ``moduleName.main.js`` acts as a bootstrap module that at it's simplest contains init code that in most cases creates an instance of ``moduleName.class.js`` : 
```
// moduleName.class.js
define(['mymodule'], function(MyModule){

	return {
		init: function() {
			var options = this.options;

			var myModule = new MyModule({
					name: options.name,
					$el: this.$el
				});
		}
	};
	
});

```

## Initializing 
There are 2 main ways to initialize modules:

In code by using ``zugzug.modules.start()``:

```
// app.main.js
require(['jquery', 'zugzug'], function($, zugzug){
	// doc ready
	$(function(){

		// call to module start
		zugzug.modules.start({
			name: 'mymodule',
			$el: $('.mymodule-container')
		});
	});	
});
```
In markup using data attributes ``data-module="mymodule"``:

```
<div class="my-module-container" data-module="mymodule">
	<!-- other markup for mymodule -->
</div>
```

Then in app.main.js using the ``zugzug.app.start()`` method:

```
// app.main.js
require(['jquery', 'zugzug'], function($, zugzug){
	// doc ready
	$(function(){
        zugzug.app
            .start()
            .then(
                function(app){
                    console.log(app.modules.get('status-module'));
                }
            );
    });	
});

```

# Use cases for Zugzug

So Far Zugzug has been used in websites with normal page loads to initialize modules. Examples include:

* nextcode.com

This architecture has be designed based on experiences in tying together functionality in .NET projects with normal page loads. Use cases that have yet to be explored are.

* Single page web applications.
* Mobile Apllications.
* php websites.