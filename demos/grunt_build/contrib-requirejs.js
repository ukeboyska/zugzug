/*global module, require*/
module.exports = function(grunt) {

	var _ = grunt.util._;

	var widgets = require('fs').readdirSync('debug/scripts/modules');

	var moduleArray = (function(){
		var moduleArrayOutput = ['app.main'];
		_.each(widgets, function(widgetDir) {
			moduleArrayOutput.push('modules/' + widgetDir + '/' + widgetDir + '.main');
		});
		return moduleArrayOutput;
	}());

	grunt.config('requirejs', {
		compile: {
			options: {
				baseUrl: 'debug/scripts',
				mainConfigFile: 'debug/scripts/require.config.js',
				inlineText: true,
				name: 'almond',
				insertRequire: ['app.main'],
				out: 'release/scripts/app.built.js',
				include: moduleArray,
				//.NET rules for removing white space but preserving line numbers
				//an not mangeling variable names
				optimize: 'uglify',
				preserveLicenseComments: false,
				uglify: {
					beautify: true,
					indent_start: 0,
					indent_level: 0,
					no_mangle: true,
					no_squeeze: true
				}
			}
		}
	});

	grunt.loadNpmTasks('grunt-contrib-requirejs');

};