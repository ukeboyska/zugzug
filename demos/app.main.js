/*global require */
// App.js - logic for starting widgets


require([
    'jquery',
    'underscore',
    'zugzug'
    ], function( $, _, zugzug ) {
    'use strict';

    $(function(){
        zugzug.app
            .start()
            .then(
                function(){
                    //console.log(app.modules.get('status-module'));
                }
            );
    });

});
